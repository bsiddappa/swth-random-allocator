#Random Allocator

This is a Java Spring Boot based tool which will provide you a random allocations for each week for the list of members provided to the allocator.

This is a maven repository and one can build the jar like below;
-mvn clean install 

This will create the jar under target folder which could be executed as below;
-java -jar <jar name>

Once successfully started, the application would be running on 8080 port (by default), Kindly verify the port from the console logs.

Do a POST RESTApi query to <host:port>/doRandomAllocation which will generate the new allocations pair
Ex URL: localhost:8080/doRandomAllocation

Set the following params while accessing the client Api;
Params-
membersFile: provide the Members List file (absolute path of tsk file)
allocationFile: previously Pair-Allocations tsk file (absolute path)

On sending the REST request, the allocator will generate the new Allocations pair list and send the same in HTTP response which can then be downloaded locally.
Also note that this new pair-allocations will be added to existing Pair-Allocations tsk file with ‘New Week -yyyy-dd-mm-hh:mm’ title for next week execution.

Next time if your send the REST request to the allocator again, you only have to provide the above files paths as previously in the request params and it should re-write the names file with new set of allocations.
