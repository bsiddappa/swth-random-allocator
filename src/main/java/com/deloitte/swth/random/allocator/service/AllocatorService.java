package com.deloitte.swth.random.allocator.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AllocatorService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public AllocatorService()  {
    }

    public File doRandomAllocation(Map<String, String> membersList, Map<String, List<String>> pairAllocation, String allocationsFile) {
        File newAllocationsFile = null;
        Map<String, String> newList = new HashMap<String, String>();
        List<String> userList = membersList.entrySet().stream().map(Map.Entry::getKey).collect(Collectors.toList());
        for (int i = 0; i < userList.size(); i++){
            List <String> tempList = new ArrayList<String>();
            tempList.addAll(userList);
            String user = userList.get(i);
            if(!newList.containsValue(user)) {
                logger.info("Searching pair for user: " + user);
                List<String> userKeyAllocations = pairAllocation.get(user);
                logger.info("Allocated users: " + userKeyAllocations);
                userKeyAllocations.addAll(newList.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList()));
                userKeyAllocations.addAll(newList.entrySet().stream().map(Map.Entry::getKey).collect(Collectors.toList()));

                userKeyAllocations.add(user);
                tempList.removeAll(userKeyAllocations);
                logger.info("Available users: " + tempList);
                if(tempList.size() > 0) {
                    newList.put(user, getRandom(tempList.stream().toArray(String[]::new)));
                }else{
                    logger.info("No more allocations for User: " +user);
                }
            }
        }
        logger.info("New Allocations: " +newList);
        // Save the new allocations in new file
        writeNewAllocationsInFile(newList, allocationsFile);
        File file = new File(allocationsFile);
        String filePath = file.getParent();
        if(newList.size() > 0) {
            newAllocationsFile = writeUserNamesInAllocationsFile(newList, membersList, filePath);
            logger.info("Successfully generated the new allocations pair list");
        }else{
            logger.info("No more allocations, all random allocations exhausted?, Kindly verify.");
        }
        return newAllocationsFile;
    }

    /*
     * Method to write the member names in new allocations file
     */
    private File writeUserNamesInAllocationsFile(Map<String, String> newList, Map<String, String> membersList, String filePath) {
        File file = new File(System.getProperty("java.io.tmpdir")+"/"+"tw-speed-connecting-allocations-names.tsv");
        FileWriter csvWriter = null;
        try {
            csvWriter = new FileWriter(file);
            csvWriter.append("Date-" + new SimpleDateFormat("yyyy-MM-dd-HH:mm").format(new Date()));
            csvWriter.append("\n");

            for (Map.Entry<String, String> entry : newList.entrySet()) {
                csvWriter.append(membersList.get(entry.getKey()));
                csvWriter.append("\t");
                csvWriter.append(membersList.get(entry.getValue()));
                csvWriter.append("\n");
            }
            csvWriter.flush();
            csvWriter.close();
            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    private void writeNewAllocationsInFile(Map<String, String> newList, String allocationsFile) {
        FileWriter csvWriter = null;
        try {
            csvWriter = new FileWriter(allocationsFile, true);
            csvWriter.append("\n");
            csvWriter.append("New Week-" + new SimpleDateFormat("yyyy-MM-dd-HH:mm").format(new Date()));
            csvWriter.append("\n");

            for (Map.Entry<String, String> entry : newList.entrySet()) {
                csvWriter.append(entry.getKey());
                csvWriter.append("\t");
                csvWriter.append(entry.getValue());
                csvWriter.append("\n");
            }
            csvWriter.flush();
            csvWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getRandom(String[] array) {
        int rnd = new Random().nextInt(array.length);
        return array[rnd];
    }

}
