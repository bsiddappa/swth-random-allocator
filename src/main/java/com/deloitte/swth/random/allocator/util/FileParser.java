package com.deloitte.swth.random.allocator.util;

import au.com.bytecode.opencsv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class FileParser {
    private final Logger logger = LoggerFactory.getLogger(FileParser.class);

    public Map readTextFile(String filePath) throws IOException {
        Map allocationsList = new HashMap<>();
        CSVReader reader = new CSVReader(new FileReader(filePath), '\t');
        String[] record;
        while ((record = reader.readNext()) != null) {
            if(!record[0].contains("New Week") && !record[0].equals("")) {
                allocationsList.put(record[0], record[1]);
            }
        }
        logger.info("User Map: " +allocationsList);
        return allocationsList;
    }

    // Get the allocattions for all users in a list
    public Map<String, List<String>> readUserAllocationTextFile(String filePath) throws IOException {
        Map allocationsList = new HashMap<String, List<String>>();
        CSVReader reader = new CSVReader(new FileReader(filePath), '\t');
        String[] record;
        while ((record = reader.readNext()) != null) {
            if(!record[0].contains("New Week") && !record[0].equals("")) {
                if(allocationsList.get(record[0]) != null){
                    ((List<String>)allocationsList.get(record[0])).add(record[1]);
                }else {
                    List<String> values = new ArrayList<>();
                    values.add(record[1]);
                    allocationsList.put(record[0], values);
                }
                if(allocationsList.get(record[1]) != null){
                    ((List<String>)allocationsList.get(record[1])).add(record[0]);
                }else {
                    List<String> values = new ArrayList<>();
                    values.add(record[0]);
                    allocationsList.put(record[1], values);
                }
            }
        }
        logger.info("Allocations Map: " +allocationsList);
        return allocationsList;
    }

}
