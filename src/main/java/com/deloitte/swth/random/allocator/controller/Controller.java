package com.deloitte.swth.random.allocator.controller;

import com.deloitte.swth.random.allocator.service.AllocatorService;
import com.deloitte.swth.random.allocator.util.FileParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
public class Controller {

    private static final Logger logger = LoggerFactory.getLogger(Controller.class);

    @Autowired
    public FileParser fileParser;

    private static AllocatorService allocatorService(){
        return new AllocatorService();
    };

    @PostMapping(value = "/doRandomAllocation")
    public ResponseEntity doRandomAllocation(@RequestParam("membersFile") String membersFile, @RequestParam("allocationFile") String allocationFile){
        logger.info("Processing");
        if(!fileExists(membersFile)){
            return ResponseEntity.ok().body("Members File does not exist");
        }
        if(!fileExists(allocationFile)){
            return ResponseEntity.ok().body("Allocations File does not exist");
        }

        try {
            logger.info("Reading the files and allocating new pairs....");
            //Read the files and get the members list and pair-allocations
            try {
                Map<String, String> memberList = fileParser.readTextFile(membersFile);
                Map<String, List<String>> allocationList = fileParser.readUserAllocationTextFile(allocationFile);
                File nFile = allocatorService().doRandomAllocation(memberList, allocationList, allocationFile);

                Resource resource = new UrlResource(nFile.toPath().toUri());                
                return ResponseEntity.ok()
                        .contentType(MediaType.parseMediaType("application/octet-stream"))
                        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + nFile.getName() + "\"")
                        .body(resource);
            } catch (IOException e) {
                e.printStackTrace();
                return ResponseEntity.status(200).body(e.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(200).body(e.getMessage());

        }
    }

    private boolean fileExists(String fileName) {
        File file = new File(fileName);
        if(file.exists()){
            return true;
        }
        return false;
    }

}
